all: main.o
	g++ main.o -std=c++11 -o main

main.o: 
	g++ -c main.cpp -std=c++11

clean:
	rm main.o main